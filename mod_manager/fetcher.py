#!/usr/bin/python3

from requests import get
from urllib.parse import urlparse
from bs4 import BeautifulSoup


class Mod_Info:

    def get_mod_download_link(
        mod_url,
        game_ver,
        sources={},
        announce=False
    ):
        source = '{uri.scheme}://{uri.netloc}/'.format(
            uri=urlparse(mod_url)
        ).strip('/')

        headers = {
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:67.0) \
                Gecko/20100101 Firefox/67.0'
        }

        resp = get(mod_url, headers=headers)

        if resp.status_code == 200:
            dom = BeautifulSoup(resp.content, "html5lib")
        else:
            return

        # Mod title
        title_expr = sources[source]['title']
        if title_expr[0] == '!':
            title = title_expr[1:]
        else:
            title_tag = dom.select(title_expr)
            title = title_tag[0].text if len(title_tag) > 0 else 'Unknown'

        # Download links
        links = []
        for row in dom.select(sources[source]['file_row']):
            ver = row.select(sources[source]['file_version'])[0].text
            if game_ver in ver:
                file_data = {}
                link_name = row.select(sources[source]['file_link'])[0]
                if sources[source]['name_from_ver']:
                    file_data['version_name'] = ver
                else:
                    file_data['version_name'] = link_name.text
                link = link_name.get('href')
                complete_url = bool(urlparse(link).netloc)
                file_data['link'] = link if complete_url else (source + link)
                links.append(file_data)

        if bool(announce):
            print('\n%s\n=====' % (title))
            if len(links) == 0:
                print('No suitable version found')
            else:
                for file_info in links:
                    print('%s | %s' % (
                        file_info.get('version_name'), file_info.get('link')
                    ))
        return {title: links}


def main():
    print()
    print('Launched directly. [DEMO mode]')

    Mod_Info.get_mod_download_link(
        'https://minecraft.curseforge.com/projects/the-biome-overhaul/files/',
        '1.14.1',
        announce=True
    )

    Mod_Info.get_mod_download_link(
        'https://www.planetminecraft.com/mod/temperature-in-minecraft/',
        '1.13',
        announce=True
    )

    Mod_Info.get_mod_download_link(
        'https://optifine.net/downloads',
        '1.14',
        announce=True
    )

    print()
    print('Done')


if __name__ == '__main__':
    main()
