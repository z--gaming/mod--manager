#!/usr/bin/python3

from setuptools import setup, find_packages

with open('./README.md', 'r') as fh:
    l_description = fh.read()

with open('./requirements.txt', 'r') as f:
    i_requires = list(map(lambda x: x.split('==')[0], f.read().splitlines()))

setup(
    name='mod_manager-mikko-timofeev',
    version='0.0.1',
    description='This is a mod manager/mod_manager',
    long_description=l_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/mikko.timofeev/mod_manager',
    author='Mikko Timofeev',
    author_email='mikko.timofeev@gmail.com',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Gamers',
        'Topic :: Gaming :: Modding Tools',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
    ],
    keywords='game mods mod_manager',
    packages=find_packages(),
    python_requires='>=3.5',
    install_requires=i_requires,
    extras_require={
        'dev': [
            'pipreqs',
        ],
        'test': [

        ],
    },
    package_data={},
    entry_points={
        'console_scripts': [
            'manager=manager:main',
        ]
    },
    project_urls={
        'Source': 'https://gitlab.com/mikko.timofeev/mod_manager'
    },
    license='MIT',
)
